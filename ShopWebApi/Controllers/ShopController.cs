﻿using ShopWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ShopWebApi.Controllers
{
    public class ShopController : ApiController
    {
        public static List<Product> ProductsInShop = new List<Product> {
            new Product {Id = 1, Name = "Apple", Amount = 500, Price = 3 },
            new Product {Id = 2, Name = "juice orange", Amount = 320, Price = 25 },
            new Product {Id = 3, Name = "Milk", Amount = 105, Price = 18 },
            new Product {Id = 4, Name = "Bride", Amount = 92, Price = 8 },
            new Product {Id = 5, Name = "Mi band 4 ", Amount = 32, Price = 600 }

        };
        // GET: api/Shop
        public List<Product> Get()
        {
            return ProductsInShop;
        }

        // GET: api/Shop/5
        public Product Get(int id)
        {
            foreach (var product in ProductsInShop)
            {
                if (product.Id ==id)
                {
                    return product;
                }
            }
            return null;
        }
        [HttpPost]
        [Route("api/Shop/buy")]
        public void Buy()
        {
            var items = new List<PurchaseItem>
            {
                new PurchaseItem{Id = 5, Amount = 3 },
                new PurchaseItem{Id = 2 , Amount = 6}
            };
            foreach (var purchaseItem in items)
            {
                foreach (var product in ProductsInShop)
                {
                    if (purchaseItem.Id == product.Id && product.Amount >= purchaseItem.Amount)
                    {
                        product.Amount -= purchaseItem.Amount;
                        break;
                    }
                }
            }
            
        }

        [HttpGet]
        [Route("api/Shop/sort")]
        public List<Product> Sort()
        {
            var result = new List<Product>();

            foreach (var item in ProductsInShop )
            {

            }


            return result;
        }



        // POST: api/Shop
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Shop/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Shop/5
        public void Delete(int id)
        {
        }
    }
}
