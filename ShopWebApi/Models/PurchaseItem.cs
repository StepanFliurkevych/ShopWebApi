﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApi.Models
{
    public class PurchaseItem
    {
        public int Id { get; set; }
        public int Amount { get; set; }  
    }
}